# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 15:35:29 2021

@author: seb
"""

import const
from Unit import Unit
import random

class Game:
    
    target = ""
    population = []
    
    def __init__(self, string):
        self.target = string
        
    def run(self):
        self.initializePopulation()
        debug = 1
        while(True):
            debug -= 1
            if(debug < 0):
                break
            
            #self.printPopForDebug("Before refresh score")
            self.refreshScore()
            #self.printPopForDebug("After refresh score")
            self.sortByScore()
            #self.printPopForDebug("After sort by score")
            self.tanos()
            #self.printPopForDebug("After Tanos's works")
            self.allIsLove()
            
            # nex : mutate in allIsLove
        
    def initializePopulation(self):
        self.population = []
        for _ in range(0, const.MAX_POPULATION):
            self.population.append(Unit(self.target, False))
            
    def getBetterScores(self):
        bestScore = 0
        
        for _ in range(0, const.MAX_POPULATION):
            print("")
            
    def tanos(self):
        lastTenPercent = int(const.MAX_POPULATION * 0.1)
        self.population = self.population[0:lastTenPercent]
        
    def sortByScore(self):
        asChange = True
        
        while(asChange):
            asChange = False
            for i in range(0, const.MAX_POPULATION-1):
                if(self.population[i].score < self.population[i+1].score):
                    asChange = True
                    temp = self.population[i+1]
                    self.population[i+1] = self.population[i]
                    self.population[i] = temp
                    
    
    def allIsLove(self):
        popSize = len(self.population)-1
        splicedAt = random.randint(0, len(self.target))
        while(len(self.population) < const.MAX_POPULATION):
            rand1 = random.randint(0, popSize)
            rand2 = random.randint(0, popSize)
            
            if(rand1 != rand2):
                newGenome = self.population[rand1].gamette(splicedAt, True)
                newGenome += self.population[rand2].gamette(splicedAt+1, False)
                self.population.append(Unit(newGenome, True))
    
    def refreshScore(self):
        for i in range(0, const.MAX_POPULATION):
            self.population[i].getScore(self.target)
            
    def printPopForDebug(self, message):
        print ("\n"+message)
        printArray = []
        for i in range(0, len(self.population)):
             printArray.append(self.population[i].score)
        print(printArray)