# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 15:10:32 2021

@author: seb
"""
import random
import const

class Unit:
    genome = ""
    score = 0
    
    def __init__(self, string, isNewBorn):
        if(not isNewBorn):
            for _ in range(len(string)):
                random_integer = random.randint(0, const.MAX_ASCII_LIMIT)
                self.genome += (chr(random_integer))
            else:
                self.genome = string     

    def getScore(self, string):
        self.score = 0
        for i in range(0, len(string)):
            if self.genome[i] == string[i]:
                self.score += 1
                
    def gamette(self, splicedAt, isStart):
       if(isStart):
           return self.genome[0:splicedAt+1]
       else:
           endGenome = len(self.genome)
           return self.genome[splicedAt:endGenome]